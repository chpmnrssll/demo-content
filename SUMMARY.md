# Summary

## komgo

* [Platform Overview](README.md)

## Know Your Customer \(KYC\)

* [What is KYC?](know-your-customer-kyc/what-is-kyc.md)
* [KYC on komgo](know-your-customer-kyc/kyc-on-komgo.md)
* [Main screens](know-your-customer-kyc/main-screens/README.md)
  * [Counterparty Docs Screen](know-your-customer-kyc/main-screens/counterparty-docs-screen.md)
  * [Document Library](know-your-customer-kyc/main-screens/document-library.md)
  * [Template Manager Screen](know-your-customer-kyc/main-screens/template-manager-screen.md)
* [KYC platform actions](know-your-customer-kyc/kyc-platform-actions/README.md)
  * [Document Request with the template \(Load Template Button\)](know-your-customer-kyc/kyc-platform-actions/document-request-with-the-template-load-template-button.md)
  * [Create Document Request Template](know-your-customer-kyc/kyc-platform-actions/create-document-request-template.md)
  * [Document Request](know-your-customer-kyc/kyc-platform-actions/document-request.md)

## Counterparty Management

* [What is Counterparty Management?](counterparty-management/what-is-counterparty-management.md)
* [Counterparty Management on komgo](counterparty-management/counterparty-management-on-komgo.md)
* [Counterparty Management Screens](counterparty-management/counterparty-management-main-screens/README.md)
  * [Counterparty Management Screen](counterparty-management/counterparty-management-main-screens/counterparty-management-screen.md)
* [Counterparty Management Platform Actions](counterparty-management/counterparty-management-platform-actions/README.md)
  * [Add a new counterparty](counterparty-management/counterparty-management-platform-actions/add-a-new-counterparty.md)
  * [Add corporate counterparties for received trade \(auto\)](counterparty-management/counterparty-management-platform-actions/add-corporate-counterparties-for-received-trade-auto.md)
  * [Review counterparty request](counterparty-management/counterparty-management-platform-actions/review-counterparty-request.md)
  * [View list of authorized counterparties](counterparty-management/counterparty-management-platform-actions/view-list-of-authorized-counterparties.md)

## FAQs - Questions and Answers

