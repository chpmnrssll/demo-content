---
product: Counterparty Management
capability: screens
infotype: Overview
audience:

---

# Counterparty Management Main Screens

The Counterparty Management screens allows users to manage their counterparty management related tasks on the platform.