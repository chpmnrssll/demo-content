---
product: Counterparty Management
capability: actions
infotype: Overview
audience:

---

# Counterparty Management Platform Actions
The following actions are available on the Counterparty Management area.